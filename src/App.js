import React from 'react';
import './App.css'
import FunctionalComp from './Components/FunctionalComp';
import { ClassComp1, ClassComp } from './Components/ClassComp';
import Click from './Components/Click';
import Counter from './Components/Counter';
import ParentComp from './Components/ParentComp';
import Classpropes from './Components/Classpropes';
import Functionprops from './Components/FunctionProps';
import State from './Components/state';

function App() {
  return <div>
    <h1>Welcome to the my First Project</h1>
    <h1>This about Component</h1>
    <FunctionalComp />
    <ClassComp />
    <ClassComp1></ClassComp1>
    <Click></Click>
    <Counter></Counter>
    <ParentComp></ParentComp>
    <Classpropes name="Learner 1" place="Chandigarh"><p>Child Component</p></Classpropes>
    <Classpropes name="Learner 2" place="Muzaffarpur"><button>Click</button></Classpropes>
    <Classpropes name="Learner 3" place="Kolkata"></Classpropes>
    <Functionprops name="Learner 1" place="Bangalore"></Functionprops>
    <State></State>
  </div>
}

export default App;
