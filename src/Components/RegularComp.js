import React, { Component } from 'react'

export class RegularComp extends Component {
  render() {
    console.log("Regular Comp Render")
    return (
      <div>
        I'm the regular Component {this.props.name}
      </div>
    )
  }
}

export default RegularComp
