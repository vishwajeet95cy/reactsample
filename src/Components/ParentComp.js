import React, { Component } from 'react'
import PureComp from './PureComp';
import RegularComp from './RegularComp';

export class ParentComp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: "Vishwa"
    }
  }
  componentDidMount() {
    setInterval(() => {
      this.setState({ name: "Vishwa" })
    }, 1000)
  }

  render() {
    console.log("Parent Comp Render")
    return (
      <div>
        I'm the parent Component
        <RegularComp name={this.state.name} />
        <PureComp name={this.state.name} />
      </div>
    )
  }
}

export default ParentComp
