import React from 'react'
import NewComp from './NewComp'


export class State extends React.Component {

  styles = {
    fontStyle: "bold",
    color: "teal",
    textAlign: "center"
  }
  render() {
    return (
      <div>
        <h1 style={this.styles}>Welcome</h1>
        <NewComp></NewComp>
      </div>
    )
  }
}

export default State
