import React, { Component } from 'react'

export class Classpropes extends Component {
  render() {
    return (
      <div>
        <h1>Hello {this.props.name} from {this.props.place}! Welcome to React Tuorial</h1>
        <p>{this.props.children}</p>
      </div>

    )
  }
}

export default Classpropes
