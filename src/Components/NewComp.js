import React, { Component } from 'react'
import belloff from './belloff.png';
import bellon from './bellon.png'

export class NewComp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      message: "Subscribe to Simplilearn",
      sub: "Subscribe",
      imageUrl: bellon
    }
  }
  styles = {
    fontStyle: "italic",
    color: "purple",
    textAlign: "center"
  }
  Buttonchange = () => {
    this.setState({
      message: "Hit the bell icon naver miss update",
      sub: "Subscribed",
      imageUrl: belloff
    })
  }

  render() {
    return (
      <div style={this.styles}>
        <h3>{this.state.message}</h3>
        <button onClick={this.Buttonchange}>{this.state.sub}</button>
        <p />
        <img style={{ width: "30px", height: "30px" }} src={this.state.imageUrl} alt="" />
      </div>
    )
  }
}

export default NewComp
