import React from 'react';


function Functionprops(props) {
  return (
    <div>
      <h2>This is functional </h2>
      <h2>Hello {props.name} from {props.place}! Welcome to React Tutorial</h2>
    </div>
  )
}

export default Functionprops;